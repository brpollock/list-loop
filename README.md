# List Loop

Finding a loop in a singly linked list.

## Build and Run

To build and run the tests:


```
make
```

If the tests perform as expected, the last line of output should be:

```
All tests OK
```

## Pipelines

To view the latest pipeline output:

1. Click **Pipelines**
2. Click the most-recent pipeline (the top one)
3. In the **Build** pane, click the `>` to the right of `make` to expand the output


## Files

```
Makefile - makefile
list.h   - list type
loop.c   - hasloop() function
main.c   - tests
```
