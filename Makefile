ALL: BUILD RUN

BUILD: main

main: main.c loop.c list.h Makefile
	gcc -Wall -o main main.c loop.c

RUN:
	./main
