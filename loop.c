/* Find loop in linked list

   Returns:
   0 - no loop
   N - loop of length N found
*/

#include <stddef.h>
#include "list.h"

size_t hasloop(link *p)
{
    link *seen     = p;
    size_t range   = 0;
    size_t current = 0;
    while(p) {
        p = p->next;
        ++current;
        if(p == seen)
            return current - (range>>1);
        if(current >= range) {
            range = current<<1;
            seen = p;
        }
    }
    return 0;
}

