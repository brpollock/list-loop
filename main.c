#include <stdio.h>
#include <stddef.h>
#include <stdlib.h>

#include "list.h"

extern size_t hasloop(link *);

void test(link *p,unsigned looplength)
{
    unsigned len = hasloop(p);
    printf(" %u",len);fflush(stdout);
    if(looplength != len) {
        printf("Expected loop length: %u\n",looplength);
        exit(1);
    }   
}

int main(void)
{
    link e[12];

    for(int n = 1;n < sizeof(e)/sizeof(e[0]);n++){
        printf("\nnum elements: %2d - loop size: ",n);
        for(int l = 0;l <= n;l++) {
            for(int i = 0;i < n;i++) {
                e[i].next = NULL;
                if(i < n - 1) {
                    e[i].next = &e[i + 1];
                } else if(l > 0) {
                    e[i].next = &e[i + 1 - l];
                }
            }
            test(&e[0], l);
        }
    }

    puts("\nAll tests OK");
    return 0;
}
